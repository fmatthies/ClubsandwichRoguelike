# ClubsandwichRoguelike
A starting point for roguelike development with [bearlibterminal](http://foo.wyrd.name/en:bearlibterminal)
(with the wrapper and convenience library [clubsandwich](https://github.com/irskep/clubsandwich) on top) and
[tcod](https://python-tcod.readthedocs.io/en/latest/).
The code is basically the first part of the
[Roguelike Tutorial Revised](http://rogueliketutorials.com/tutorials/tcod/part-1/) where you learn to draw an `@`
character and move it around the screen, but implemented with `bearlibterminal` and the `SceneManager` framework of
 `clubsandwich`.  

### MainLoop
The MainLoop is started in the last line and it automatically calls its `terminal_init` and `get_initial_scene` methods.
With both `terminal.set()` calls the tileset is loaded and the game window configured. I created a separate python file
(`config.py`) that holds all the information necessary for the former; it can be rewritten to suit your needs, even use
another tileset (see section **Tileset Drawing**).

### GameScreen
The GameScreen holds basically all the information that is indented under the `while` loop in the
[Roguelike Tutorial Revised](http://rogueliketutorials.com/tutorials/tcod/part-1/). I just moved the `player_x` and
`player_y` variables into it and therefore they need to be referenced with `self`. I also moved the `handle_keys`
function to the file `utilities.py` that holds another function I will explain later.  
`terminal_read` is a method that will be called every time there is keyboard/mouse input. `terminal_update` on the other
hand will be called every frame. This is why the former method holds the logic for key press detection and the latter is
responsible for the 'drawing'.

### Tileset Drawing
`utilities.py` also contains the function `read_tiles`. This is a convenience function around specifying a tile from a
tileset. `bearlibterminal` allows for assigning a complete tileset to a starting code point in the "Unicode Multiplane"
and it handles (given the right arguments) the slicing and assignment of individual tiles to each consecutive code point
by itself. You would now be able to tell `bearlibterminal` which tile to "`terminal.put()`" to the screen by providing
the appropriate code point. That might be feasible with a small tileset, but is quite a hassle otherwise.  
That's where `read_tiles` comes into play: it takes as input either a string or an integer in the following form
(or lists thereof):

1. "1:3" (coordinate-string)
2. "0x1010" (hex-string)
3. "98" (integer-string)
4. 98 (integer)

I won't talk about 2. - 4. as 1. is probably all you need and the most convenient one. The string you provide is
basically just a coordinate on the complete tileset. So in this example, you tell the function to return the codepoint
for the fourth tile (3) in the second row (1); rows and columns count start at 0 each.  
To be more precise `read_tiles`'s return value is a list of code points, because you can also provide a list as
argument (as mentioned). The reason for this is just future proofing, so you can easily declare multiple tiles for
"one entity" - to realize animation or just have different representations of the same (e.g. different tree tiles
depending on state or some such).  
So to step through the "real world example" in `line 49` in `engine.py`:  
`terminal.put(self.player_x, self.player_y, read_tiles(Tiles.player_char)[0])`  
* `terminal.put` is `bearlibterminal`'s way of drawing a character (tile or character) to the screen, similar to
`libtcod`'s `console_put_char`.
* next come the x and y coordinates of the player character
* and lastly the character or code point that should be drawn: in this case the first value (`[0]`) of the list returned
by `read_tiles(Tiles.player_char)`
* `Tiles.player_char` is defined as "`1:0`" in `config.py`.
* So the input of `read_tiles` instructs it to look for the first tile in the second row of the `arial10x10.png` tileset
(the `@`) that was also defined in `config.py` (as `Tiles.tile_set`).

---
## ToDo

### Why Bearlib?
* layer
* composition

### Why Clubsandwich?
* Director
