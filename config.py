# -*- coding: utf-8 -*-


class Tiles:
    # Unicode Multiplane Private Use Areas: 0xE000 - 0xF8FF -> 6,400 code points
    code_sp = "0xE000"
    # any tileset might be used here: tile_size and tile_set_width need to be changed accordingly
    tile_set = "static/png/arial10x10.png"
    # the size of a tile in the tileset in px with width and height
    tile_size = (10, 10)
    # the number of tile slots per row
    tile_set_width = 32

    # a coordinate (row:column) in the tileset for the tile (row and column start each at 0)
    player_char = "1:0"


class Window:
    # width and height of the screen
    window_size = (80, 50)
