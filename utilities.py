# -*- coding: utf-8 -*-

from clubsandwich.blt.state import blt_state
from config import Tiles


def handle_keys():
    # Movement keys
    if blt_state.up:
        return {'move': (0, -1)}
    elif blt_state.down:
        return {'move': (0, 1)}
    elif blt_state.left:
        return {'move': (-1, 0)}
    elif blt_state.right:
        return {'move': (1, 0)}

    if blt_state.shift and blt_state.enter:
        # Shift+Enter: toggle full screen
        return {'fullscreen': True}

    if blt_state.escape:
        # Exit the game
        return {'exit': True}

    # No key was pressed
    return {}


def read_tiles(char):
    start_cp = int(Tiles.code_sp, 16)
    if type(char) is list:
        tile_list = []
        for in_char in char:
            tile_list += read_tiles(in_char)
        return tile_list

    if type(char) is str:
        tile_chords = char.split(":")
        if len(tile_chords) == 1:
            s = tile_chords[0]
            if s.startswith('0x'):
                h_int = int(s, 16)
                if h_int >= start_cp:
                    return [h_int]
                else:
                    return [start_cp + h_int]
            else:
                return [start_cp + int(s)]
        elif len(tile_chords) == 2:
            # first is row, second is column
            try:
                f_chord, s_chord = tile_chords
                if f_chord.startswith('0x') or s_chord.startswith('0x'):
                    raise ValueError
                else:
                    return [start_cp + (int(f_chord) * Tiles.tile_set_width) + int(s_chord)]
            except ValueError:
                print("String Tile Representation is not an integer: '{}, {}'\n" +
                      "Please provide either an absolute hex/decimal positional value " +
                      "or a decimal coordinate value.".format(tile_chords[0], tile_chords[1]))
        else:
            print("String Tile representation could not be parsed: '{}'.\n" +
                  "Either a single value or a coordinate value in the form 'row:column'.".format(char))
    elif type(char) is int:
        return [start_cp + char]
