# -*- coding: utf-8 -*-

import os

from clubsandwich.director import DirectorLoop, Scene
from clubsandwich.blt.nice_terminal import terminal

from config import Tiles, Window
from utilities import handle_keys, read_tiles


class MainLoop(DirectorLoop):
    def terminal_init(self):
        terminal.set("{}: {}, size={}x{}"
                     .format(Tiles.code_sp, os.path.abspath(Tiles.tile_set), Tiles.tile_size[0], Tiles.tile_size[1]))
        terminal.set("window: size={}x{}, cellsize={}x{}"
                     .format(Window.window_size[0], Window.window_size[1], Tiles.tile_size[0], Tiles.tile_size[1]))
        super().terminal_init()

    def get_initial_scene(self):
        return GameScreen()


class GameScreen(Scene):
    player_x = int(Window.window_size[0] / 2)
    player_y = int(Window.window_size[1] / 2)

    def terminal_read(self, val):
        # called whenever input is detected
        action = handle_keys()

        move = action.get('move')
        fullscreen = action.get('fullscreen')
        game_exit = action.get('exit')

        if move:
            dx, dy = move
            self.player_x += dx
            self.player_y += dy

        if game_exit:
            self.director.pop_scene()  # GameScreen is the bottom-most scene, so 'popping' it closes it all

        if fullscreen:
            print("FullScreen!")  # no fullscreen; just key combination confirmation (shift+enter)

    def terminal_update(self, is_active=False):
        # this is called every frame
        terminal.put(self.player_x, self.player_y, read_tiles(Tiles.player_char)[0])

        return True  # don't delete this


if __name__ == "__main__":
    MainLoop().run()
